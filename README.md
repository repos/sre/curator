Curator
===

[Original README](README.orig.rst)

Building
---

Must be built on the intended target distribution.  A vagrant VM or docker image is recommended.

Replace contents of [VERSION](VERSION) with intended Debian-formatted version number. E.g. `5.8.5-1~wmf3`
```shell
apt update
apt install -y ruby ruby-dev rubygems build-essential rsync python3-venv python3-dev zlib1g-dev
gem install --no-document fpm
./build.sh
```

Resulting package will be found in `/tmp/curator_packages/`
