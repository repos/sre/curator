#!/bin/bash
set -xe

# Can change these
PYVER=$(python3 -c 'import sys; print(f"{sys.version_info.major}.{sys.version_info.minor}")')
PYPATCH=$(python3 -c 'import sys; print(sys.version_info.micro)')
OPENSSL_VER=$(openssl version | cut -d ' ' -f 2)
CX_FREEZE_VER='6.11.1'
DEB_VER=$(cat VERSION)

# Don't change these
PKG_TARGET=/tmp/curator_packages
WORKDIR=/tmp/curator_workdir
VENVDIR=/tmp/curator_python
SRCDIR=$(pwd)
INPUT_TYPE=python
CATEGORY=python
VENDOR=Elastic
MAINTAINER="'Elastic Developers <info@elastic.co>'"
C_POST_INSTALL=${WORKDIR}/es_curator_after_install.sh
C_PRE_REMOVE=${WORKDIR}/es_curator_before_removal.sh
C_POST_REMOVE=${WORKDIR}/es_curator_after_removal.sh
C_PRE_UPGRADE=${WORKDIR}/es_curator_before_upgrade.sh
C_POST_UPGRADE=${WORKDIR}/es_curator_after_upgrade.sh


# Build our own package pre/post scripts
rm -rf ${WORKDIR} ${VENVDIR} /opt/elasticsearch-curator
mkdir -p ${WORKDIR} ${PKG_TARGET}

#/usr/local/bin/python?.? --version
#Python 3.9.4
RAWPYVER=$(python3 --version | awk '{print $2}')
PYVER=$(echo $RAWPYVER | awk -F\. '{print $1"."$2}')
MINOR=$(echo $RAWPYVER | awk -F\. '{print $3}')

for file in ${C_POST_INSTALL} ${C_PRE_REMOVE} ${C_POST_REMOVE}; do
  echo '#!/bin/bash' > ${file}
  echo >> ${file}
  chmod +x ${file}
done

for binary_name in curator curator_cli es_repo_mgr; do
  for script_target in ${C_POST_INSTALL} ${C_POST_UPGRADE}; do
    echo "echo '#!/bin/bash' > /usr/bin/${binary_name}" >> ${script_target}
    echo "echo >> /usr/bin/${binary_name}" >> ${script_target}
    echo "echo -n LD_LIBRARY_PATH=/opt/elasticsearch-curator/lib /opt/elasticsearch-curator/${binary_name} >> /usr/bin/${binary_name}" >> ${script_target}
    echo "echo ' \"\$@\"' >> /usr/bin/${binary_name}" >> ${script_target}
    echo "chmod +x /usr/bin/${binary_name}" >> ${script_target}
  done
  for script_target in ${C_PRE_REMOVE} ${C_PRE_UPGRADE}; do
    echo "rm -f /usr/bin/${binary_name}" >> ${script_target}
    echo "rm -f /etc/ld.so.conf.d/elasticsearch-curator.conf" >> ${script_target}
    echo "ldconfig" >> ${script_target}
  done
done

echo 'if [ -d "/opt/elasticsearch-curator" ]; then' >> ${C_POST_REMOVE}
echo '  rm -rf /opt/elasticsearch-curator' >> ${C_POST_REMOVE}
echo 'fi' >> ${C_POST_REMOVE}

# build
if [ "${DEB_VER}x" == "x" ]; then
  echo "Must provide version number (can be arbitrary)"
  exit 1
else
  GIT_PATH=$(pwd)
fi

# Build virtualenv
mkdir -p ${VENVDIR}/${PYVER}.${MINOR}
cd ${VENVDIR}
python3 -m venv ${PYVER}.${MINOR}
source ${VENVDIR}/${PYVER}.${MINOR}/bin/activate
pip install wheel
pip install cx_freeze==${CX_FREEZE_VER} patchelf-wrapper

# Install pre-requisites
cd ${GIT_PATH}
pip install -r requirements.txt
python setup.py build_exe

mv build/exe.linux-x86_64-${PYVER} /opt/elasticsearch-curator
chown -R root:root /opt/elasticsearch-curator
cd $WORKDIR

for pkgtype in deb; do
  fpm \
   -s dir \
   -t ${pkgtype} \
   -n elasticsearch-curator \
   -v ${DEB_VER} \
   --vendor ${VENDOR} \
   --maintainer "${MAINTAINER}" \
   --license 'Apache-2.0' \
   --category tools \
   --url 'https://gitlab.wikimedia.org/repos/sre/curator' \
   --description 'Have indices in Elasticsearch? This is the tool for you!
Like a museum curator manages the exhibits and collections on display, Elasticsearch Curator helps you curate, or manage your indices.
This is a fork of the upstream project using the opensearch-py client library.
' \
   --after-install ${C_POST_INSTALL} \
   --before-remove ${C_PRE_REMOVE} \
   --after-remove ${C_POST_REMOVE} \
   --before-upgrade ${C_PRE_UPGRADE} \
   --after-upgrade ${C_POST_UPGRADE} \
   --provides elasticsearch-curator \
   --conflicts python-elasticsearch-curator \
   --conflicts python3-elasticsearch-curator \
  /opt/elasticsearch-curator
  mv ${WORKDIR}/*.${pkgtype} ${PKG_TARGET}
done

deactivate

rm ${C_POST_INSTALL} ${C_PRE_REMOVE} ${C_POST_REMOVE} ${C_PRE_UPGRADE} ${C_POST_UPGRADE}
rm -rf ${VENVDIR}
